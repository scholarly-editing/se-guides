# Scholarly Editing Guides

This repository collects guides for editors and contributors to Scholarly Editing.

All Guides are public domain [CC0](https://creativecommons.org/publicdomain/zero/1.0/).
