export const SITE = {
	title: 'Scholarly Editing Guides',
	description: 'Guides for editors and contributors to Scholarly Editing',
	defaultLanguage: 'en-us',
} as const;

export const OPEN_GRAPH = {
	image: {
		src: 'https://scholarlyediting.org/static/se-title-709758088f4810055d12c1877115b315.png',
		alt:
			'Scholarly Editing Logo, with the larger and nested initials.' +
			' The word "editing" is written over two lines, with "edit" highlighted in red.',
	},
	twitter: 'raffazizzi',
};

export const KNOWN_LANGUAGES = {
	English: 'en',
} as const;
export const KNOWN_LANGUAGE_CODES = Object.values(KNOWN_LANGUAGES);

export const GITHUB_EDIT_URL = `https://gitlab.com/scholarly-editing/se-guides/-/tree/main`;

export const COMMUNITY_INVITE_URL = ``;

export type Sidebar = Record<
	(typeof KNOWN_LANGUAGE_CODES)[number],
	Record<string, { text: string; link: string }[]>
>;
export const SIDEBAR: Sidebar = {
	en: {
		'General': [
			{ text: 'Publishing a new issue', link: '/en/publishing-new-issue' },
			{ text: 'Updating OJS', link: '/en/updating-ojs' },
		],
		'Micro-Editions': [
			{ text: 'Getting started', link: '/en/microedition-getting-started' },
			{ text: 'Configuring VS Code', link: '/en/microedition-vscode' },
		],
	},
};
