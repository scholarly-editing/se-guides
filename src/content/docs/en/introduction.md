---
title: "Scholarly Editing Guides"
description: "Docs intro"
---

This repository collects guides for editors and contributors to [Scholarly Editing](https://scholarlyediting.org).

All Guides are public domain [CC0](https://creativecommons.org/publicdomain/zero/1.0/).

