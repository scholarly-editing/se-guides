---
title: "Getting started with the microedition staging site"
description: "Getting started with the microedition staging site"
---

## Install VS Code

Install [VSCode](https://code.visualstudio.com/). You will need this to work on the site and to edit the TEI unless you prefer to use other software such as Oxygen. If you would like to set up VSCode for TEI editing follow [this guide](https://gitlab.com/scholarly-editing/se-guides/-/blob/main/vscode.md).

## Set up Git and GitLab

You should have received an invite to set up a [GitLab](https://gitlab.com) account and join a Scholarly Editing Microedition project.

### If you know Git

Simply clone the site repository on your computer and skip down to Run the website locally.

### If you’re new to Git

If you have not used Git before, it is a versioning system that will allow us to collaborate on the files that make the staging site. You will need to install Git on your computer, you can find instructions for several operating systems on the [Git website](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).

After Git is installed, you can start using it via VSCode. These are a [few slides (from a UMD class)](https://docs.google.com/presentation/d/1wnB9FGofrkYteGfoUny9HH6ezDT5W3ilpQI4Xpng4o4/edit#slide=id.g901c70b983_0_57) that can help you get started.

* Look at slides 20-27 for a general introduction to Git and GitLab
* Follow the steps on slides 31 and 32 to "clone" the site on your computer using VSCode
* Make a change to the TEI file and "commit" and "push" it to send it to GitLab so that the Technical Editor can receive your changes. See slides 35-37.

## Run the website locally

### Install Node.js and npm

The website is written in JavaScript, which needs Node.js to run on your computer. [Download and install Node.js](https://nodejs.org/en/download/) and its package manager npm. On Windows, this should be relatively straightforward. On MacOS you may run into issues with npm later. If you have a problem, contact the Technical Editor for assistance.

### Install dependencies

If you’re familiar with the command line, go to the cloned directory and `npm install`. If you're familiar with Node.js, install `yarn` and use `yarn install` to take advantage of the `yarn.lock` file.

Otherwise:

* Open the site directory in VSCode from the menu `File > Open Folder`. If you cloned from GitLab with VSCode, you may have this directory already opened.
* Open a terminal from the menu `Terminal > New Terminal`.
* In the terminal type `npm install` and press Enter.

### Run the site

If you're familiar with the command line, type `npm run develop`. If you're familiar with Node.js, use `yarn` and type `yarn develop`.

Otherwise:

* Make sure your site directory is open in VSCode (see above)
* Open a terminal from the menu `Terminal > New Terminal`.
* In the terminal type `npm run develop` and press Enter.

After a little while, the staging site will be available via the browser at http://localhost:8000. While VSCode is open, the website should update itself after each change.
