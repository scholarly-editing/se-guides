---
title: "Downloading and Configuring VS Code for TEI editing"
description: "VS Code configuration"
---

_Based on a guide for Atom by Jessica Lu and Raffaele Viglianti_

VS Code is a text editor with support for a number of programming and markup languages. It is free and open source. Through an extension, it can be used to validate XML files against a schema—for example to make sure the file being edited follows TEI rules. The same extension also offers autocompletion suggestions, which makes it easier to figure out which TEI elements and attributes to use.

This document will guide you through a number of steps to install and configure VS Code.

## 1. Download VS Code

VS Code can be downloaded at https://code.visualstudio.com/download. Versions are available for Windows, Mac, and Linux. Select and install the appropriate version for your operating platform, as you would any other application.

## 2. Add extensions to VS Code

* Open VS Code and click on the extension tab

  ![extensions tab icon](vscodeext.png)

  situated on the left side of the window.
* In the search bar, enter “scholarly”
* Among the search results, you’ll find Scholarly XML. Click on the green install button. The process should take a few seconds.

  ![scholarly xml extension](scholarlyxml.png)

## 3. (Optional) Test Scholarly XML extension

* In VS Code, open a new file from the main menu bar: “File” → “New File.”
* Copy and paste the TEI template below to test the plug-in.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="https://vault.tei-c.org/P5/current/xml/tei/custom/schema/relaxng/tei_lite.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://vault.tei-c.org/P5/current/xml/tei/custom/schema/relaxng/tei_lite.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xmlns:xi="http://www.w3.org/2001/XInclude">
 <teiHeader>
     <fileDesc>
        <titleStmt>
           <title>A title</title>
        </titleStmt>
        <publicationStmt>
           <p/>
        </publicationStmt>
        <sourceDesc>
           <p/>
        </sourceDesc>
     </fileDesc>
 </teiHeader>
 <text>
   <body>
     <p/>
   </body>
 </text>
</TEI>
```

_The first lines before `<TEI>` indicate that this is an XML file, and associate this file with the latest TEI schema available online._

* Navigate to “File” → “Save As…” to save the file with an “.xml” extension to activate the plugin (e.g. “test.xml”). Perform the operations below to make sure the plug-in is working properly.
* Check for error messages (indicated by a red-colored circle along the left-hand side) when the XML is not “well formed”, e.g. by removing a closing tag.

  ![Scholarly XML error](sxmlerror.png)

* Start typing a new attribute or element to get autocompletion suggestions.

  ![Scholarly XML error](sxmlsuggestion.png)
