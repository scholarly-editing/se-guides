---
title: "Publishing a New Scholarly Editing Issue"
description: "All the steps"
---

* Start by adding a new entry to issues.json. Copying over the same content and updating. Setting DOI to zeros for now and desc to "Blurb."

* This isn't enough to get the new issue page to show up; if there still isn't content available, create a mock md file.

* The TOC is derived from the files themselves, as they indicate which section they belong to; but the JSON file sets the section order.

* pages/issues.tsx updates automatically, but still worth checking all is in order.  

## Converting copy-edited files to Markdown

Use pandoc.

`pandoc -s essay.docx --wrap=none --atx-headers -t markdown_strict-smart+footnotes -o author.md`

Copy to /src/contents/issues/issue_number

Copy a frontmatter from previous issue, or another file from the same issue and update.
Then correct output, or get the assistant technical editor to help.

Watch out: reviews are a special case. Follow the structure of a review from previous issues.

Images go to /images/issues/NO/AUTHOR/IMAGE.jpg

Table captions: ```<figcaption>Table 1: <em>Urban In-sights: A Workshop in American Visual Culture and Literacy</em>—Original and Modified Budgets, 2019–2020.</figcaption>```

## Integrating micro-editions

* Check ODD and TEI files for micro-editions
* Adjust micro-editions readme

Each micro-edition needs to be built separately and the static files added to /static/issues/NO/SLUG/

1. Make sure metadata in gatsby-config.js is complete and up to date. E.g. 
   ```
   issue: {
      full: "Volume 40",
      short: "Vol 40",
      path: "/issues/40"
   }
   ```
   Remember the check the group order as well.
2. generate metadata file with `yarn metadata` to get a slug for the edition.
3. Update package.json `build-deploy` PATH with issue number and generated slug.
4. Update .gitlab-ci.yml variable STATICPATH with the slug.
5. Push to GitLab and trigger deploy-to-prepub pipeline manually
6. Alternatively to 5, copy the generated public folder to prepub. Rename it to the slug value found in metadata.json

Noticed that DOIs need to be different for micro-editions to appear on the TOC, so pick something different even when testing.

## Organizing content

Add the new issue to /src/contents/issues/issues.json. The name of sections must match metadata in each file. Group order is determined by the files metadata, but it can also be expressed with "contents" value for each section. While this is optional, this redundancy can be valuable to get an idea of the TOC directly from issues.json.

Note for future: it would be good to have a script that generates this info from the files.

## Generating DOIs

Generate dois for micro-editions using `yarn doi`. The script will output a DOI that you'll have to copy manually to gastby-config.

ONLY GENERATE THE ISSUE DOIs ONCE THE MICRO-EDITIONS ARE INTEGRATED. The metadata files provided by the micro-editions will be needed.

For the issue, use `yarn dois VOLUMENUMBER`. If you had an example DOI, you'll have to use `--force` to replace it.

This should also generate and XML file. 

Test XML file in crossref at https://test.crossref.org/
Look on the queue to find the submission. The submitter field is `adei`.
To find the results look at Submissions > Administration and send an empty query. Make sure there are no errors or fix the metadata until you're good.

Once you have no errors, submit to the real thing at https://doi.crossref.org/

Check the queue in the same way. Once it's done, or close to being done, publish!

## Publishing

It should be as simple as merging prepub into main. See the README file.

## After publication

Set micro-editions repositories to archived (read only)
