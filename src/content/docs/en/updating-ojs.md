---
title: "Updating OJS"
description: "What a fun process /s"
---

* Make a backup of the Database on CPanel, you will likely need it
  > if you need to restore the database do not delete it or you'll run into permission problems that the kind Reclaim Hosting support folks will have to fix. Instead, DROP all tables and then restore the DB. This will keep the MySQL user untouched.

* Download the latest OJS from https://pkp.sfu.ca/software/ojs/download/
  This video is helpful (currently upgrading from 3.3.x to 3.3.x) https://www.youtube.com/watch?v=NF4bcefTrMk

* `public_html` is the current OJS folder. All the content from there needs to move to a different folder and be replaced by the contents of the zipped file your downloaded (the latest OJS). **IMPORTANT**:

  > KEEP THESE FOLDERS IN `public_html`: `se-archive`, `se-assets`, and `public`.

* Diff old `config.inc.php` and new one and make sure to move over DB credentials and other necessary stuff.
* Make sure that `Installed` is set to `Off` in `config.inc.php`.
* Visit https://submissions.scholarlyediting.org/
* Follow prompts for updating existing version.

  If you run into an error 500 (or a white page of death), set `Debug` to `On` in order to see the error.
  Encountered a UTF error. Found out on PHP my admin that the database was set to Swedish (??), switched it to UTF-8. Hopefully this won't be a problem in the future.

  I also ran into foreign key errors. I had to switch all the table's storage engine from InnoDB to MyISAM. To do this, I edited the database backup SQL file and replaced all occurences of `ENGINE=InnoDB` with `ENGINE=MyISAM`.
  > WARNING: MyISAM is apparently going to be deprecated soon. I predict that this will come back to bite us.

* Set `Installed` back to `On` in `config.inc.php`.

Done!

